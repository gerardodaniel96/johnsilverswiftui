//
//  John_SilverApp.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import SwiftUI

@main
struct John_SilverApp: App {
    var body: some Scene {
        WindowGroup {
            FormView()
        }
    }
}
