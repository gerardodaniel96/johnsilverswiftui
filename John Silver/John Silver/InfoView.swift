//
//  InfoView.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import SwiftUI

struct InfoView: View {
    let userInfo: FormData
    
    var body: some View {
        VStack {
            Image(systemName: "person.fill")
                .resizable()
                .frame(width: 100, height: 100)
            Text(userInfo.name + " " + userInfo.lastName)
                .font(.title)
                .padding()
            Text(userInfo.phone)
                .padding()
            Text(userInfo.email)
        }
        .padding()
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView(userInfo: FormData(name: "John", lastName: "Silver", phone: "(81) 2267 3446", email: "test@banregio.com"))
    }
}
