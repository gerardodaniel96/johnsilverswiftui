//
//  ContentView.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import SwiftUI

struct FormView: View {
    
    @State private var name: String = ""
    @State private var lastName: String = ""
    @State private var phone: String = ""
    @State private var email: String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section {
                        TextField("Nombre", text: self.$name)
                        TextField("Apellido", text: self.$lastName)
                        TextField("Teléfono", text: self.$phone)
                        TextField("Correo electrónico", text: self.$email)
                    } header: {
                        HStack {
                            Spacer()
                            Image(systemName: "person.fill.questionmark")
                                .resizable()
                                .frame(width: 100, height: 100)
                                .padding()
                            Spacer()
                        }
                        Text("User info")
                    } footer: {
                        HStack {
                            Spacer()
                            NavigationLink("Send", destination: InfoView(userInfo: FormData(name: self.name, lastName: self.lastName, phone: self.phone, email: self.email)))
                                .padding()
                                .font(.largeTitle)
                            Spacer()
                        }
                    }

                }
                .headerProminence(.increased)
            }
            .navigationTitle("Form")
            .navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        FormView()
    }
}
