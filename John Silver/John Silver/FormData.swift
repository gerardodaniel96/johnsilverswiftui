//
//  FormData.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import Foundation

struct FormData {
    var name: String
    var lastName: String
    var phone: String
    var email: String
}
